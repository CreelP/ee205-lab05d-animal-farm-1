////////////
///////////////////// University of Hawaii, College of Engineering
///////////////////// @brief Lab 05b - Animalfarm0 - EE 205 - Spr 2022
/////////////////////
///////////////////// @file main.c
///////////////////// @version 1.0
/////////////////////
///////////////////// @author Creel Patrocinio <creel@hawaii.edu>
///////////////////// @date 02/20/2022
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////



#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "catDatabase.h"
#include "addCats.h"
#include "reportCats.h"
#include "updateCats.h"
#include "deleteCats.h"

#include <assert.h>
#include "config.h"
#define MAX_NAME1 "1234567890123456789012345678901234567890123456789"
#define MAX_NAME2 "DIFFERENT 123456789012345678901234567890123456789"
#define ILLEGAL_NAME "12345678901234567890123456789012345678901234567890"




int main(void) {
   printf("Starting Animal Farm 0\n\n\n");

   initializeCatDatabase();


   printf("Adding cats to database! \n");
   addCat( "Loki", MALE, PERSIAN, true, 8.5 ) ;
   addCat( "Milo", MALE, MANX, true, 7.0 ) ;
   addCat( "Bella", FEMALE, MAINE_COON, true, 18.2 ) ;
   addCat( "Kali", FEMALE, SHORTHAIR, false, 9.2 ) ;
   addCat( "Trin", FEMALE, MANX, true, 12.2 ) ;
   addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0 ) ;
 
   printf("Cats added to database\n\n");
   printAllCats();


   int kali = findCat( "Kali" );
   printf("This is Kali's location in our Database\n", catName[kali]);
   printCat(kali);

   printf("Attempting to set to duplicate name\n");
   updateCatName( kali, "Chili" );
      
   printCat( kali );
           
      
   printf("Done with Animal farm 0\n");
      
   return 0;
      
}
