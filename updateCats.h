////////////
///////////////// University of Hawaii, College of Engineering
///////////////// @brief Lab 05b - Animalfarm0 - EE 205 - Spr 2022
/////////////////
///////////////// @file updateCats.h
///////////////// @version 1.0
/////////////////
///////////////// @author Creel Patrocinio <creel@hawaii.edu>
///////////////// @date 02/20/2022
///////////////////////////////////////////////////////////////////////////////////////////
///////////
//
#pragma once

#include "catDatabase.h"
#include "addCats.h"
#include <string.h>


bool updateCatName(const size_t index, const char *newName);
bool fixCat(const size_t index);
bool updateCatWeight(const size_t index, const float weight);
bool updateCollar1(const size_t index, const enum Color newColor);
bool updateCollar2(const size_t index, const enum Color newColor);
bool updateLicense(const size_t index, const unsigned long long int newLicense);



