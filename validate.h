/////////////////////////////////////////////////////////////////////////////
//////////////
/////////////// University of Hawaii, College of Engineering
/////////////// @brief Lab 05b - Animalfarm0 - EE 205 - Spr 2022
///////////////
/////////////// @file validate.h
/////////////// @version 1.0
///////////////
/////////////// @author Creel Patrocinio <creel@hawaii.edu>
/////////////// @date 02/20/2022
/////////////////////////////////////////////////////////////////////////////////////////
//////////
////
//

#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "catDatabase.h"


bool validName(const char *name);
bool validWeight(const float weight);
bool validIndex(const size_t index);
bool validcollarColor(const enum Color collar1, 
                     Color collar2);

bool validLicense(const unsigned long long licenseNum);






